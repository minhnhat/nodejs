'use strict';

const path = require('path');
const express = require('express');

const app = express();

app.use(express.static(path.resolve('./public')));

app.route('/api')
  .get((req, res) => {
    res.status(200).send('What do you want ?');
  })

module.exports = app;