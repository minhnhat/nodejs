'use strict';

const app     = require('./app');
const fs      = require('fs');
const path    = require('path');
const request = require('supertest');

describe('/', () => {
  describe('GET', () => {
    it('should get 200 : file<index.html>', done => {
      request(app)
        .get('/')
        .expect(200)
        .expect(fs.readFileSync(path.resolve('./public/index.html')).toString(), done)
    })
  })
});

describe('/api', () => {
  describe('GET', () => {
    it('should get 200 : What do you want ?', done => {
      request(app)
        .get('/api')
        .expect(200)
        .expect('What do you want ?', done);
    })
  })
});